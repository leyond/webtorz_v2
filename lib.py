import os
import shutil
import hashlib
import base64
import uuid
import zipfile

def deleteFolder(folder):
	try:
		shutil.rmtree(folder)
	except OSError:
		pass

def createFolder(folder,deleteExisting=False):
	if deleteExisting:
		deleteFolder(folder)

	try:
		os.makedirs(folder)
	except OSError:
		pass

def md5File(filename):
	md5 = None

	with open(filename,'rb') as file:
		m = hashlib.md5()

		for chunk in iter(lambda: file.read(4096), b''):
			m.update(chunk)

		md5 = m.hexdigest()

	return md5

def getFileSize(filename):
	try:
		size = os.path.getsize(filename)
	except OSError:
		size = None

	return size

def isFile(filename):
	return os.path.isfile(filename)

def isFolder(folder):
	return os.path.isdir(folder)

def fixPassword(password):
	return base64.b64encode(hashlib.sha256(password.encode('utf-8')).digest())

def getUniqueID():
	return uuid.uuid4().hex

def createZipFile(zip_filename,folder):
	folder_name = os.path.basename(folder)

	with zipfile.ZipFile(zip_filename,'w',compression=zipfile.ZIP_DEFLATED) as zf:
		for root, dirs, files in os.walk(folder):
			for filename in files:
				real_filename = os.path.normpath(os.path.join(root,filename))
				arcname = os.path.join(folder_name,filename)

				zf.write(real_filename,arcname)