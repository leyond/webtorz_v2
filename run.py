from concurrent.futures import ThreadPoolExecutor
from tornado import gen
from tornado.web import url, authenticated
from tornado_sqlalchemy import SessionMixin, make_session_factory
from db import User
from lib import deleteFolder, createFolder, md5File, getFileSize, isFile, fixPassword, createZipFile, getUniqueID
import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.httpserver
import json
import re
import os
import uuid
import libtorrent
import time
import bcrypt
import ssl
import shutil
import config

DEBUG = config.DEBUG
PORT = config.PORT
TORRENT_PORT = config.TORRENT_PORT
SSL_CERTIFICATE_FILE = config.SSL_CERTIFICATE_FILE
SSL_PRIVATEKEY_FILE = config.SSL_PRIVATEKEY_FILE
DOWNLOAD_FOLDER = config.DOWNLOAD_FOLDER
TEMP_FOLDER = config.TEMP_FOLDER
TORRENT_WORKERS = config.TORRENT_WORKERS
DATABASE_URL = config.DATABASE_URL
SECRET_KEY = config.SECRET_KEY
CHUNK_SIZE = config.CHUNK_SIZE

session = libtorrent.session({'listen_interfaces': '0.0.0.0:{}'.format(TORRENT_PORT)})

torrentExecutor = ThreadPoolExecutor(TORRENT_WORKERS)

torrents = {}

createFolder(DOWNLOAD_FOLDER)
createFolder(TEMP_FOLDER)

def create_torrent(url):
	global session, torrents

	uid = getUniqueID()
	folder = os.path.join(TEMP_FOLDER,'webtorz_download_{}'.format(uid))

	handler = session.add_torrent({
		'url': url,
		'save_path': folder,
		'flags': libtorrent.add_torrent_params_flags_t.flag_paused
	})

	torrents[uid] = {
		'handler': handler,
		'future': None
	}

	return uid

def download_torrent(uid):
	global session, torrents

	handler = torrents[uid]['handler']
	folder = handler.save_path()

	handler.resume()

	while not handler.is_seed() and uid in torrents:
		time.sleep(1)

	if uid in torrents:
		name = handler.name()

		info_hash = str(handler.info_hash())

		savefolder = os.path.join(DOWNLOAD_FOLDER,info_hash)

		total_size = 0

		files_info = {}
		for root, dirs, files in os.walk(folder):
			for f in files:
				filename = os.path.join(root,f)
				md5 = md5File(filename)
				if md5:
					size = getFileSize(filename)

					pattern = os.path.join(re.escape(folder),'(.*)')
					search = re.search(re.compile(pattern),filename)
					if search:
						total_size += size

						filename = search.group(1)

						files_info[filename] = {
							'md5': md5,
							'size': size
						}

		info_filename = os.path.join(folder,'torrent_info.json')
		with open(info_filename,'w') as file:
			json.dump({
				'info_hash': info_hash,
				'name': name,
				'size': total_size,
				'files': files_info
			},file,indent=4)

		deleteFolder(savefolder)
		shutil.move(folder,savefolder)

		torrents.pop(uid,None)
		session.remove_torrent(handler)

	deleteFolder(folder)

def add_torrent(url):
	global torrentExecutor, torrents

	uid = create_torrent(url)

	future = torrentExecutor.submit(download_torrent,uid)
	torrents[uid]['future'] = future

	return uid

def delete_torrent(uid):
	global session, torrents

	if uid in torrents and torrents[uid]['future']:
		torrent = torrents.pop(uid)

		handler = torrent['handler']
		future = torrent['future']

		folder = handler.save_path()

		session.remove_torrent(handler)
		future.cancel()

		deleteFolder(folder)
	else:
		torrent = None

	return torrent

class BaseHandler(tornado.web.RequestHandler, SessionMixin):
	def get_current_user(self):
		return self.get_secure_cookie('user')

	def prepare(self):
		self.xsrf_token

class MainHandler(BaseHandler):
	@authenticated
	def get(self):
		self.render('main.html')

class FilesHandler(BaseHandler):
	@authenticated
	def get(self):
		self.render('files.html')

class ApiKeyHandler(BaseHandler):
	@authenticated
	def get(self):
		username = self.get_current_user().decode('utf-8')

		results = {
			'username': username,
			'api_key': None
		}

		with self.make_session() as session:
			user = session.query(User).get(username)

			if user:
				results['api_key'] = user.api_key

		self.write(json.dumps(results))

class TorrentListHandler(BaseHandler):
	@authenticated
	def get(self):
		results = []

		for key in torrents:
			torrent = torrents[key]
			handler = torrent.get('handler',None)
			future = torrent.get('future',None)

			if handler and future:
				name = handler.name()
				status = handler.status()

				if handler.is_paused():
					state = 0
				else:
					state = status.state

				results.append({
					'uid': key,
					'info_hash': str(handler.info_hash()),
					'name': name,
					'download_rate': status.download_rate,
					'upload_rate': status.upload_rate,
					'peers': status.num_peers,
					'state': state,
					'total_wanted_done': status.total_wanted_done,
					'total_wanted': status.total_wanted
				})

		self.write(json.dumps(results))

class TorrentAddHandler(BaseHandler):
	@authenticated
	def post(self):
		try:
			data = json.loads(self.request.body)
		except:
			data = None

		uid = None

		if data:
			url = data.get('url',None)

			if url:
				if url.startswith('magnet:?'):
					hashes = []

					pattern = re.compile(r'xt(?:\\.\d+)?=urn:(?:[a-z0-9]+)?:([A-Z0-9]+)',re.IGNORECASE)
					for match in pattern.finditer(url):
						hashes.append(match.group(1))

					if hashes:
						found = False

						for key in torrents:
							handler = torrents[key]['handler']
							info_hash = str(handler.info_hash())

							for h in hashes:
								if h == info_hash:
									found = True
									break

							if found:
								break

						if not found:
							uid = add_torrent(url)

		results = {
			'uid': uid
		}

		self.write(json.dumps(results))

class TorrentDeleteHandler(BaseHandler):
	@authenticated
	def post(self):
		try:
			data = tornado.escape.json_decode(self.request.body)
		except:
			data = None

		success = False

		if data:
			uid = data.get('uid',None)

			if uid:
				success = delete_torrent(uid) != None

		results = {
			'success': success
		}

		self.write(json.dumps(results))

class FileListHander(BaseHandler):
	@authenticated
	def get(self):
		results = []

		with os.scandir(DOWNLOAD_FOLDER) as it:
			for entry in it:
				if entry.is_dir():
					name = os.path.join(DOWNLOAD_FOLDER,entry.name)
					info_filename = os.path.join(name,'torrent_info.json')
					if isFile(info_filename):
						with open(info_filename,'r') as file:
							info = json.load(file)

							info_hash = info.get('info_hash',None)
							name = info.get('name',None)
							size = info.get('size',None)

							if info_hash and name and 'files' in info:
								results.append({
									'info_hash': info_hash,
									'name': name,
									'size': size
								})

		self.write(json.dumps(results))

class FileDeleteHandler(BaseHandler):
	@authenticated
	def post(self):
		try:
			data = tornado.escape.json_decode(self.request.body)
		except:
			data = None

		success = False

		if data:
			info_hash = data.get('info_hash',None)

			if info_hash:
				folder = os.path.join(DOWNLOAD_FOLDER,info_hash)
				info_filename = os.path.join(folder,'torrent_info.json')
				if isFile(info_filename):
					with open(info_filename,'r') as file:
						info = json.load(file)

						if info.get('info_hash',None) == info_hash:
							success = True
							deleteFolder(folder)

		results = {
			'success': success
		}

		self.write(json.dumps(results))

class FileViewHandler(BaseHandler):
	@authenticated
	def get(self,info_hash=None):
		files = []

		if info_hash:
			folder = os.path.join(DOWNLOAD_FOLDER,info_hash)
			info_filename = os.path.join(folder,'torrent_info.json')
			if isFile(info_filename):
				with open(info_filename,'r') as file:
					info = json.load(file)

					if info.get('info_hash',None) == info_hash:
						files = info.get('files',{})

		results = {
			'info_hash': info_hash,
			'files': files
		}

		self.write(json.dumps(results))

class FileDownloadHandler(BaseHandler):
	def initialize(self):
		self.temp_folder = None

	def on_finish(self):
		deleteFolder(self.temp_folder)

	async def get(self):
		authenticated = False

		if self.get_current_user():
			authenticated = True
		else:
			api_key = self.get_argument('apikey',None)

			if api_key:
				with self.make_session() as session:
					user = session.query(User).filter_by(api_key=api_key).first()

					if user:
						authenticated = True

		if authenticated:
			info_hash = self.get_argument('info_hash',None)
			path = self.get_argument('path',None)

			filename = None

			if info_hash:
				folder = os.path.join(DOWNLOAD_FOLDER,info_hash)
				info_filename = os.path.join(folder,'torrent_info.json')

				if isFile(info_filename):
					with open(info_filename,'r') as file:
						info = json.load(file)

					if path:
						files = info.get('files',{})
						file = files.get(path,None)

						if file:
							filename = os.path.join(folder,path)
					else:
						name = info.get('name')

						if name:
							temp_folder = os.path.join(TEMP_FOLDER,'webtorz_archive_{}'.format(getUniqueID()))

							self.temp_folder = temp_folder

							createFolder(temp_folder)

							filename = '{}.zip'.format(os.path.join(temp_folder,name))

							await tornado.ioloop.IOLoop.current().run_in_executor(None,createZipFile,filename,os.path.join(folder,name))

			if filename and isFile(filename):
				size = getFileSize(filename)

				if size:
					self.set_header('Content-Length',size)

				self.set_header('Content-Type','application/octet-stream')
				self.set_header('Content-Disposition','attachment; filename={}'.format(os.path.basename(filename)))

				with open(filename,'rb') as f:
					while True:
						chunk = f.read(CHUNK_SIZE)
						if not chunk:
							break

						try:
							self.write(chunk)
							await self.flush()
						except:
							break
						finally:
							del chunk
							await gen.sleep(0.000000001)
			else:
				self.set_status(500)
		else:
			self.set_status(403)

class LogoutHandler(BaseHandler):
	@authenticated
	def get(self):
		self.clear_cookie('user')
		self.redirect(self.application.reverse_url('main'))

class LoginHandler(BaseHandler):
	def get(self):
		if self.get_current_user():
			self.redirect(self.application.reverse_url('main'))
		else:
			self.render('login.html')

	def post(self):
		try:
			data = tornado.escape.json_decode(self.request.body)
		except:
			data = None

		authenticated = False

		if data:
			username = data.get('username',None)
			password = data.get('password',None)

			if username and password:
				with self.make_session() as session:
					user = session.query(User).get(username)

					if user:
						password_hash = user.password

						if password_hash and bcrypt.checkpw(fixPassword(password),password_hash.encode('utf-8')):
							authenticated = True
							self.set_secure_cookie('user',username)
        
		results = {
			'authenticated': authenticated
		}

		self.write(json.dumps(results))

def make_app():
	factory = make_session_factory(DATABASE_URL)

	settings = {
		'debug': DEBUG,
		'template_path': 'templates',
		'static_path': 'static',
		'cookie_secret': SECRET_KEY,
		'xsrf_cookies': True,
		'login_url': '/login'
	}

	return tornado.web.Application([
		url(r'/', MainHandler, name='main'),
		url(r'/files', FilesHandler, name='files'),
		url(r'/apikey', ApiKeyHandler, name='apikey'),
		url(r'/torrent/list', TorrentListHandler, name='torrent_list'),
		url(r'/torrent/add', TorrentAddHandler, name='torrent_add'),
		url(r'/torrent/delete', TorrentDeleteHandler, name='torrent_delete'),
		url(r'/file/list', FileListHander, name='file_list'),
		url(r'/file/delete', FileDeleteHandler, name='file_delete'),
		url(r'/file/view', FileViewHandler, name='file_view'),
		url(r'/file/view/([a-zA-Z0-9]+)', FileViewHandler),
		url(r'/file/download', FileDownloadHandler, name='file_download'),
		url(r'/logout', LogoutHandler, name='logout'),
		url(r'/login', LoginHandler, name='login'),
	],session_factory=factory,**settings)

if __name__ == '__main__':
	if SSL_CERTIFICATE_FILE and SSL_PRIVATEKEY_FILE:
		ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
		ssl_ctx.verify_mode = ssl.CERT_NONE
		ssl_ctx.load_cert_chain(SSL_CERTIFICATE_FILE,SSL_PRIVATEKEY_FILE)
	else:
		ssl_ctx = None

	server = tornado.httpserver.HTTPServer(make_app(),ssl_options=ssl_ctx)
	server.listen(PORT)
	tornado.ioloop.IOLoop.current().start()