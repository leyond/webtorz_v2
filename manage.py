from db import session_scope, User
from lib import fixPassword
import argparse
import re
import sys
import getpass
import bcrypt
import hashlib
import time
import uuid

def getUsers():
	users = {}

	with session_scope() as session:
		for user in session.query(User).all():
			username = user.username
			password = user.password
			api_key = user.api_key

			users[username] = {
				'password': password,
				'api_key': api_key
			}

	return users

def addUser(username,password):
	password_hash = bcrypt.hashpw(fixPassword(password),bcrypt.gensalt()).decode('utf-8')

	m = hashlib.sha256()
	m.update(username.encode('utf-8'))
	m.update(uuid.uuid4().hex.encode('utf-8'))
	m.update(str(time.time()).encode('utf-8'))

	api_key = m.hexdigest()

	with session_scope() as session:
		user = User(username=username,password=password_hash,api_key=api_key)
		session.add(user)

def deleteUser(username):
	with session_scope() as session:
		user = session.query(User).get(username)

		if user:
			session.delete(user)

parser = argparse.ArgumentParser()
parser.add_argument('option', help='createuser, deleteuser, listusers')

args = parser.parse_args()

option = args.option
if option == 'createuser':
	username = None

	while not username:
		try:
			user_input = input('Enter a username (\'q\' to quit): ')
		except KeyboardInterrupt:
			print()
			sys.exit(0)

		if user_input:
			if user_input == 'q':
				sys.exit(0)

			users = getUsers()
	
			if user_input in users:
				print('Username already exists')
			else:
				if re.search(r'^[0-9a-z]+$',user_input):
					username = user_input
				else:
					print('Username can only contain alphanumeric characters')

	try:
		password = getpass.getpass('Password: ')
		confirmpassword = getpass.getpass('Confirm Password: ')
	except KeyboardInterrupt:
		print()
		sys.exit(0)

	if password == confirmpassword:
		addUser(username,password)
		print('User "{}" created'.format(username))
	else:
		print('Error: Passwords do not match')
elif option == 'deleteuser':
	username = None

	while not username:
		try:
			user_input = input('Enter a username (\'q\' to quit): ')
		except KeyboardInterrupt:
			print()
			sys.exit(0)

		if user_input:
			if user_input == 'q':
				sys.exit(0)

			username = user_input

	users = getUsers()

	if username in users:
		deleteUser(username)
		print('User "{}" deleted'.format(username))
	else:
		print('Error: User "{}" not found'.format(username))
elif option == 'listusers':
	users = getUsers()

	for username in users:
		print(username)