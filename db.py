from tornado_sqlalchemy import declarative_base
from sqlalchemy import create_engine, Column, String
from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager
import config

DATABASE_URL = config.DATABASE_URL

engine = create_engine(DATABASE_URL)

DeclarativeBase = declarative_base()

Session = sessionmaker()

Session.configure(bind=engine)

@contextmanager
def session_scope():
	session = Session()

	try:
		yield session
		session.commit()
	except:
		session.rollback()
		raise
	finally:
		session.close()

class User(DeclarativeBase):
	__tablename__ = 'user'

	username = Column(String(255),primary_key=True)
	password = Column(String(60),nullable=False)
	api_key = Column(String(64),unique=True,nullable=False)

DeclarativeBase.metadata.create_all(engine)